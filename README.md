# Träwelling to GPX export

This script exports all statuses from [Träwelling](https://traewelling,de)
to GPX files.

## How to use
### Installing traewelling-gpx

You can install `traewelling-gpx` via pipx directly from this repository:

```shell
pipx install git+https://codeberg.org/Natureshadow/traewelling_gpx
```

Alternatively, you can of course clone the repository, and run the script
directly. It needs the Python packages `requests`, `typer`, and `gpx` to run.

### Getting a Träwelling API token

Next, you need an API token for Träwelling. You can get one on the
[API tokens](https://traewelling.de/settings/security/api-tokens) settings page
when logged in.

### Running traewelling-gpx

The script takes two arguments: the API token, and an optional output
directory:

```shell
traewelling-gpx --token XXXXX… --output-dir ~/traewelling_statuses
```

Only statuses that do not yet exist as GPX in the target directory will be
downloaded.

## License, author, and project information

The script was made as a quick and dirty exporter for my archive, and for
feeding into my [PostGIS](https://postgis.net/) database. That said, I did
not care to do proper error handling in most places, and in general it is
of rather hacky quality.

It is licensed under the Apache License 2.0.
